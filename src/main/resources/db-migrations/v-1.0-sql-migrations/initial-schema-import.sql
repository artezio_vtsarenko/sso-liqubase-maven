--liquibase formatted sql

--changeset vtsarenko:initial-schema-import-1
create table public.user (
    id serial primary key,
    login varchar(50) not null,
    password varchar(50) not null
);
--rollback drop table public.user;


--changeset vtsarenko:initial-schema-import-2
create table token (
    id serial primary key,
    value varchar(50) not null unique,
    timeout int not null
);
--rollback drop table token;


--changeset vtsarenko:initial-schema-import-3
create table app (
    id serial primary key,
    name varchar(50) not null,
    URL varchar(2048) not null
);
--rollback drop table app;


--changeset vtsarenko:initial-schema-import-4
create table role (
    id serial primary key,
    title varchar(50) not null,
    app_id int not null,
    constraint fk_role_app foreign key (app_id) references app(id)
);
--rollback drop table role;


--changeset vtsarenko:initial-schema-import-5
create table user_role (
    user_id int not null,
    role_id int not null,
    constraint fk_user_role_user foreign key (user_id) references public.user(id),
    constraint fk_user_role_role foreign key (role_id) references role(id)
);
--rollback drop table user_role;


--changeset vtsarenko:add-token_id-column-6
alter table public.user
add token_id int;

alter table public.user
add constraint fk_user_token foreign key (token_id) references token(id);
--rollback alter table public.user drop constraint fk_user_token; alter table public.user drop column token_id;

--changeset vtsarenko:create-index-token_id-7
create index token_id_index on public.user(token_id);
--rollback drop index token_id_index;

