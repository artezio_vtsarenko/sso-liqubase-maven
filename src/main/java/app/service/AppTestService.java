package app.service;

import app.dao.AppDAO;
import app.entity.AppEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppTestService {
    @Autowired
    AppDAO appDAO;
    public List<AppEntity> findAll() {
        return appDAO.findAll();
    }
}
