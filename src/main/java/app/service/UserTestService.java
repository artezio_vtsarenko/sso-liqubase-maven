package app.service;

import app.dao.UserDAO;
import app.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserTestService {
    @Autowired
    UserDAO userDAO;

    public List<UserEntity> findAll() {
        return userDAO.findAll();
    }
}
