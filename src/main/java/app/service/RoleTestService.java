package app.service;

import app.dao.RoleDAO;
import app.entity.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleTestService {
    @Autowired
    RoleDAO roleDAO;
    public List<RoleEntity> findAll() {
        return roleDAO.findAll();
    }
}
