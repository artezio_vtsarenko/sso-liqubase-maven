package app.controller.testControllers;

import app.service.UserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserTestController {
    @Autowired
    UserTestService service;

    @GetMapping("/list")
    String userList(Model model){
        model.addAttribute("users", service.findAll());
        return "user/list";
    }
}
