package app.controller.testControllers;

import app.service.AppTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app")
public class AppTestController {
    @Autowired
    private AppTestService appTestService;

    @GetMapping("/list")
    public String appList(Model model) {
        model.addAttribute("apps", appTestService.findAll());
        return "app/list";
    }
}
