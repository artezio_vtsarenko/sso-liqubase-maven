package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity(name="app")
@Table(name="app")
public class AppEntity extends BaseEntity<Integer>{
    @Column
    private String name;
    private String url;

    @OneToMany(mappedBy = "app")
    private List<RoleEntity> roles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void addRole(RoleEntity role) {
        role.setApp(this);
        roles.add(role);
    }

    public void removeRole(RoleEntity role) {
        roles.remove(role);
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setApps(List<RoleEntity> roles) {
        this.roles = roles;
    }
}
