package app.entity;

import javax.persistence.*;
import java.util.Set;

@Entity(name= "user")
@Table (name="user")
public class UserEntity extends BaseEntity<Integer>{

    private String login;
    private String password;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name="token_id", referencedColumnName = "id")
    private TokenEntity token;

    @ManyToMany
    @JoinTable(name="user_role",
    joinColumns = @JoinColumn(name="user_id"),
    inverseJoinColumns = @JoinColumn(name="role_id"))
    private Set<RoleEntity> roles;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TokenEntity getToken() {
        return token;
    }

    public void setToken(TokenEntity token) {
        token.setUser(this);
        this.token = token;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public void addRole(RoleEntity role) {
        role.addUser(this);
        roles.add(role);
    }

    public void removeRole(RoleEntity role) {
        role.removeUser(this);
        roles.remove(role);
    }
}
