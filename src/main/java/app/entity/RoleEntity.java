package app.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name="role")
@Table(name="role")
public class RoleEntity extends BaseEntity<Integer>{
    @Column
    private String title;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="app_id")
    private AppEntity app;

    @ManyToMany(mappedBy = "roles")
    private List<UserEntity> users;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AppEntity getApp() {
        return app;
    }

    public void setApp(AppEntity app) {
        this.app = app;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public void addUser(UserEntity user) {
        users.add(user);
    }

    public void removeUser(UserEntity user) {
        users.remove(user);
    }
}
