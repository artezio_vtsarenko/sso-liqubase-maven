package app.dao;

import app.entity.TokenEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenDAO extends DAO<TokenEntity, Integer>{
    @Override
    @Query("select t from token t")
    List<TokenEntity> findAll();

    @Override
    @Query("select t from token t where t.id=:id")
    TokenEntity findById(@Param("id") Integer id);

    @Override
    @Modifying
    @Query("delete from token t where t.id = :id")
    void delete(@Param("id") Integer id);
}
