package app.dao;

import app.entity.TokenEntity;
import app.entity.UserEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAO extends DAO<UserEntity, Integer>{
    @Override
    @Query("select u from user u")
    List<UserEntity> findAll();

    @Override
    @Query("select u from user u where u.id = :id")
    UserEntity findById(@Param("id") Integer id);

    @Override
    @Modifying
    @Query("delete from user u where u.id = :id")
    void delete(@Param("id") Integer id);

    @Query("select u from user u where u.token = token ")
    UserEntity findByToken(@Param("token") TokenEntity token);

}
