package app.dao;

import app.entity.AppEntity;
import app.entity.RoleEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDAO extends DAO<RoleEntity, Integer> {
    @Override
    @Query("select r from role r")
    List<RoleEntity> findAll();

    @Override
    @Query("select r from role r where r.id=:id")
    RoleEntity findById(@Param("id") Integer id);

    @Override
    @Modifying
    @Query("delete from role r where r.id=:id")
    void delete(@Param("id") Integer id);

    @Query("select r from role r where r.app = :app")
    List<RoleEntity> findByAppId(@Param("app") AppEntity app);
}
