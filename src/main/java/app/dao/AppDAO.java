package app.dao;

import app.entity.AppEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;


@Repository
public interface AppDAO extends DAO<AppEntity, Integer> {
    @Override
    @Query("select a from app a")
    List<AppEntity> findAll();

    @Override
    @Query("select a from app a where a.id = :id")
    AppEntity findById(@Param("id") Integer id);

    @Override
    @Modifying
    @Query("delete from app a where a.id = :id")
    void delete(@Param("id") Integer id);
}
