package app.dao;

import app.entity.BaseEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;

@NoRepositoryBean
public interface DAO<T, ID> extends Repository<T, ID> {
    List<T> findAll();
    T findById(ID id);
//    void create(T entity);
//    void update(T entity);
    void delete(ID id);
}

